using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AirplanesLib;
//validation has been done at Skyfireball account on 23.03.2018
namespace AirplanesWinForm
{
    public partial class AirplanesWinForm : Form
    {
        public static AirplanesFactory fact = new AirplanesFactory();
        public static Airline airline = fact.getAirline(AirplanesFactory.Str.norma);
        public Airplanes elem;

        public AirplanesWinForm()
        {
            InitializeComponent();
            Load += AirplanesWinForm_Load;
        }

        private void AirplanesWinForm_Load(object sender, EventArgs e)
        {
         this.FormBorderStyle = FormBorderStyle.FixedDialog;
         generate();
        }

        public void generate()
        {
            listAirplanes.Items.Clear();
            foreach (var item in airline.Airplanes)
            {
                listAirplanes.Items.Add(item.Name + " " + item.Seats);
            }
            //вызов калькулятора
            SeatsCalculator calculator = new SeatsCalculator();
            int seatsSumm = calculator.getSummSeats(airline);             
            lblSeatsTotal.Text = seatsSumm.ToString();
        }

        //кнопка добавления нового самолёта
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AirplaneAddForm form = new AirplaneAddForm();
            form.editForm = 0;
            form.LoadFormAdd();
            form.ShowDialog();

            if (form.Result == DialogResult.OK)
            {
                Airplanes airplanes = form.Airplanes;
                this.listAirplanes.Items.Add(airplanes);
                airline.Add(airplanes);
                generate();
            }

        }

        //кнопка редактирования свойств самолёта
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (listAirplanes.SelectedItem != null)
            {
                string[] text = listAirplanes.SelectedItem.ToString().Split(' ');
                AirplaneAddForm form = new AirplaneAddForm();
                form.editForm = 1;
                foreach (var item in airline.Airplanes)
                {
                    if (item.Name.Contains(text[0].ToString()))
                    {
                        form.elem = item;
                        break;
                    }
                }
                form.LoadFormAdd();
                form.ShowDialog();

                if (form.elem == null) return;
                for (int i = 0; i <= airline.Airplanes.Count(); i++)
                {
                    if (form.elem.Name == airline.Airplanes[i].Name)
                    {
                        airline.Airplanes[i].Name = form.elem.Name;
                        airline.Airplanes[i].Type = form.elem.Type;
                        airline.Airplanes[i].Range = form.elem.Range;
                        airline.Airplanes[i].Seats = form.elem.Seats;
                        airline.Airplanes[i].Speed = form.elem.Speed;
                        break;
                    }
                }
                generate();
                
            }
        }
        //кнопка удалния самолёта
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (listAirplanes.SelectedItem != null)
            {
                string[] text = listAirplanes.SelectedItem.ToString().Split(' ');
                int summ = Convert.ToInt32(lblSeatsTotal.Text);
                int rem = Convert.ToInt32(text[1]);
                listAirplanes.Items.Remove(listAirplanes.SelectedItem);
                lblSeatsTotal.Text = (summ - rem).ToString();
            }
        }        
    }
}
