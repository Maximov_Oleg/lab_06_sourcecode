using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AirplanesLib;

namespace AirplanesWinForm
{
    public partial class AirplaneAddForm : Form
    {
        public int editForm = 0;
        public Airplanes elem = null;

        public AirplaneAddForm()
        {
            InitializeComponent();
        }

        public void LoadFormAdd()
        {
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            Type ourtype = typeof(Airplanes); // Базовый тип
            IEnumerable<Type> list = Assembly.GetAssembly(ourtype).GetTypes().Where(type => type.IsSubclassOf(ourtype));  // using DietLib.##

            foreach (Type itm in list)
            {
                className.Items.Add(itm.ToString());
            }

            if (this.editForm == 1 && elem != null)
            {
                this.nameTextBox.Text = elem.Name;
                this.typeTextBox.Text = elem.Type.ToString();
                this.rangeTextBox.Text = elem.Range.ToString();
                this.seatsTextBox.Text = elem.Seats.ToString();
                this.speedTextBox.Text = elem.Speed.ToString();
            }
            //nameTextBox.Text = list.ToString();
        }


        private Airplanes _airplanes;
        public Airplanes Airplanes
        {
            get { return _airplanes; }
            set { _airplanes = value; }
        }

        private DialogResult _result;
        public DialogResult Result
        {
            get { return _result; }
            set { _result = value; }
        }


        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                switch (editForm)
                {
                    case 1:
                        if (elem == null) return;
                        elem = new Boeing737();
                        elem.Name = nameTextBox.Text;
                        elem.Type = typeTextBox.Text; // != null ? Convert.ToInt32(typeTextBox.Text) : 0;
                        elem.Range = rangeTextBox.Text != null ? Convert.ToInt32(rangeTextBox.Text) : 0;
                        elem.Seats = seatsTextBox.Text != null ? Convert.ToInt32(seatsTextBox.Text) : 0;
                        elem.Speed = speedTextBox.Text != null ? Convert.ToInt32(speedTextBox.Text) : 0;
                        break;
                    case 0:
                        Airplanes = new Boeing737();
                        Airplanes.Name = nameTextBox.Text;
                        Airplanes.Type = typeTextBox.Text; // != null ? Convert.ToInt32(proteinTextBox.Text) : 0;
                        Airplanes.Range = rangeTextBox.Text != null ? Convert.ToInt32(rangeTextBox.Text) : 0;
                        Airplanes.Seats = seatsTextBox.Text != null ? Convert.ToInt32(seatsTextBox.Text) : 0;
                        Airplanes.Speed = speedTextBox.Text != null ? Convert.ToInt32(speedTextBox.Text) : 0;
                        break;
                }
                Result = DialogResult.OK;
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
